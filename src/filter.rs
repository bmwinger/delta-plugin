/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2023 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::plugin::Plugin;
use crate::record::FieldAccess;
use crate::record::{MetaRecord, RecordId, RecordType, RecordTypeName};
use crate::serialize::{load_all, load_all_from_cfg};
use anyhow::{Context, Result};
use fancy_regex::Regex;
use hashlink::LinkedHashMap;
use std::collections::{HashMap, HashSet};
use std::path::Path;

// TODO: this should share code with the delta merge in serialize.rs
pub fn vanilla_merge(plugins: Vec<Plugin>) -> Result<LinkedHashMap<RecordId, RecordType>> {
    let mut record_map: LinkedHashMap<RecordId, RecordType> = LinkedHashMap::new();

    for plugin in plugins {
        trace!(
            "Adding plugin {} to merged data...",
            plugin.file.as_path().file_name().unwrap().to_str().unwrap()
        );
        for (id, record) in plugin.records.into_iter() {
            match record {
                MetaRecord::Delta(delta) => {
                    if let Some(master) = record_map.get_mut(&id) {
                        master
                            .get_full_record_mut()
                            .apply_patch(MetaRecord::Delta(delta));
                    } else {
                        warn!("Encountered Delta record when the masters have no record with the id! {:?}", id);
                    }
                }
                MetaRecord::Record(inner) => {
                    match inner {
                        // For CELL and DIAL/INFO records, master records are
                        // created by combining the versions in all the masters, not
                        // overriding them
                        RecordType::Dialogue(dialogue) if record_map.contains_key(&id) => {
                            if let Some(RecordType::Dialogue(old)) = record_map.get_mut(&id) {
                                old.apply(&dialogue);
                            } else {
                                unreachable!()
                            }
                        }
                        RecordType::Cell(cell) if record_map.contains_key(&id) => {
                            if let Some(RecordType::Cell(old)) = record_map.get_mut(&id) {
                                old.apply(&cell);
                            } else {
                                unreachable!()
                            }
                        }
                        record => {
                            record_map.insert(id, record);
                        }
                    }
                }
                MetaRecord::Delete => {
                    if record_map.remove(&id).is_none() {
                        warn!(
                            "Encountered delete record {:?} with no record to delete!",
                            id
                        );
                    }
                }
                MetaRecord::Unknown => (),
            }
        }
    }
    Ok(record_map)
}

#[derive(Debug)]
pub struct Filter {
    pub record_type: RecordTypeName,
    pub id: Option<Regex>,
    // If specified, only cells containing cellrefs matching this regex will be produced, and they
    // will only contain the matching cellrefs
    pub cellref_object_id: Option<Regex>,
    pub delete: bool,
    pub modify: Option<(String, Regex, String)>,
}

#[derive(Debug)]
pub enum FilterInput {
    All,
    Plugins(Vec<std::path::PathBuf>),
}

pub fn filter_record(filter: &Filter, id: &RecordId, record: &RecordType) -> Option<MetaRecord> {
    // If an id filter is specified, only match records with a matching id
    // Ignore for records without string ids
    if let Some(id_regex) = &filter.id {
        if !id_regex.is_match(&id.canonical_id()).unwrap_or(false) {
            return None;
        }
    }
    // Filter record cellrefs
    let record: RecordType = if let Some(object_id_regex) = &filter.cellref_object_id {
        if filter.record_type == RecordTypeName::Cell {
            if let RecordType::Cell(cell) = record {
                let mut new_cell = cell.clone();
                new_cell.references = new_cell
                    .references
                    .into_iter()
                    .filter_map(|(id, cellref)| {
                        use crate::delta::Deletable;
                        if let Deletable::Add(cellref) = cellref {
                            if object_id_regex
                                .is_match(&cellref.object_id.to_lowercase())
                                .unwrap_or(false)
                            {
                                Some((
                                    id,
                                    if filter.delete {
                                        Deletable::Delete
                                    } else if let Some((field, regex, new)) = &filter.modify {
                                        if let Some(field_value) = cellref.get_string_field(field) {
                                            // FIXME: Proper error handling
                                            let mut cellref = cellref.clone();
                                            cellref
                                                .set_string_field(
                                                    field,
                                                    regex
                                                        .replace_all(field_value, new)
                                                        .into_owned(),
                                                )
                                                .expect("Invalid field name!");
                                            Deletable::Add(cellref)
                                        } else {
                                            Deletable::Add(cellref)
                                        }
                                    } else {
                                        Deletable::Add(cellref)
                                    },
                                ))
                            } else {
                                None
                            }
                        } else {
                            None
                        }
                    })
                    .collect();
                if new_cell.references.is_empty() {
                    return None;
                }
                RecordType::Cell(new_cell)
            } else {
                unreachable!()
            }
        } else {
            warn!(
                "Ignoring object id filter for record type {} which is not a Cell",
                filter.record_type
            );
            record.clone()
        }
    } else if let Some((field, regex, new)) = &filter.modify {
        let mut record = record.clone();
        if let Some(field_value) = record.get_string_field(field) {
            record
                .set_string_field(field, regex.replace_all(field_value, new).into_owned())
                .expect("Invalid field name!");
        }
        record
    } else {
        record.clone()
    };
    if filter.delete && filter.cellref_object_id.is_none() {
        Some(MetaRecord::Delete)
    } else {
        Some(MetaRecord::Record(record))
    }
}

pub fn get_ordered_plugins_from_input(
    input: FilterInput,
    ignored_plugins: HashSet<String>,
) -> Result<Vec<Plugin>> {
    let records = match input {
        FilterInput::All => {
            let (plugin_names, mut plugin_map, errors) = load_all_from_cfg(false, ignored_plugins)?;
            if !errors.is_empty() {
                error!("The Following Errors occured when parsing plugins! Execution will continue, however these plugins will be skipped. The output may not be as expected as a result.");
                for error in errors {
                    error!("{:?}", error);
                }
            }
            plugin_names
                .iter()
                .flat_map(|name| plugin_map.remove(name))
                .collect()
        }
        FilterInput::Plugins(paths) => {
            let mut errors = vec![];
            let mut plugin_map = HashMap::new();
            for (name, plugin) in load_all(&paths, false) {
                match plugin {
                    Ok(plugin) => {
                        plugin_map.insert(name, plugin);
                    }
                    Err(error) => {
                        errors.push(error);
                    }
                }
            }
            if !errors.is_empty() {
                error!("The Following Errors occured when parsing plugins!");
                for error in errors {
                    error!("{:?}", error);
                }
            }
            paths
                .iter()
                .map(|path| {
                    let filename = path.file_name().unwrap().to_str().unwrap();
                    plugin_map.remove(&filename.to_lowercase()).unwrap()
                })
                .collect()
        }
    };
    Ok(records)
}

fn get_type_from_id(id: &RecordId) -> RecordTypeName {
    match id {
        RecordId::String(typename, _) => *typename,
        RecordId::SkillType(_) => RecordTypeName::Skill,
        RecordId::MagicEffect(_) => RecordTypeName::MagicEffect,
        RecordId::ExteriorCell(_) => RecordTypeName::Cell,
        RecordId::Landscape(_) => RecordTypeName::Landscape,
        RecordId::ExteriorPathGrid(_) => RecordTypeName::PathGrid,
    }
}

pub fn match_plugin(plugin: &Plugin, filters: &Vec<Filter>) -> bool {
    for (id, record) in &plugin.records {
        if let MetaRecord::Record(record) = record {
            // If any filter matches, keep the data
            // Note: If multiple filters match, only the first will apply
            for filter in filters {
                if filter.record_type == get_type_from_id(id)
                    && filter_record(filter, id, record).is_some()
                {
                    return true;
                }
            }
        }
    }
    false
}

pub fn filter_records(
    ordered_plugins: Vec<Plugin>,
    filters: &Vec<Filter>,
) -> Result<LinkedHashMap<RecordId, MetaRecord>> {
    let plugin_data = vanilla_merge(ordered_plugins)?;

    let filtered_records: Vec<(RecordId, MetaRecord)> = plugin_data
        .into_iter()
        .filter_map(|(id, record)| {
            // If any filter matches, keep the data
            // Note: If multiple filters match, only the first will apply
            for filter in filters {
                if filter.record_type == get_type_from_id(&id) {
                    if let Some(record) = filter_record(filter, &id, &record) {
                        return Some((id, record));
                    }
                }
            }
            None
        })
        .collect();

    Ok(filtered_records.into_iter().collect())
}

pub fn filter_plugin(
    input: FilterInput,
    output: &Path,
    filters: Vec<Filter>,
    author: Option<String>,
    desc: Option<String>,
    ignored_plugins: HashSet<String>,
) -> Result<Plugin> {
    let mut output = Plugin::blank(output.to_path_buf());
    if let Some(author) = author {
        output.header.author = author;
    }
    if let Some(desc) = desc {
        output.header.desc = desc;
    }

    let ordered_plugins = get_ordered_plugins_from_input(input, ignored_plugins)?;

    // Before filtering, run a query on each individual plugin and ignore any which don't
    // have matching plugins. This lets us reduce the length of the master list, since cell
    // references can only work with up to 255 plugins
    let ordered_plugins: Vec<Plugin> = ordered_plugins
        .into_iter()
        .filter(|plugin| match_plugin(plugin, &filters))
        .collect();

    let mut masters = vec![];
    for plugin in &ordered_plugins {
        let master = crate::record::header::Master::new(plugin).context(format!(
            "In plugin {}",
            &plugin.file.file_name().unwrap().to_str().unwrap()
        ))?;
        masters.push(master);
    }
    output.records = filter_records(ordered_plugins, &filters)?;
    // Serialization may require having an accurate master list
    // If the plugin contains cellreferences
    output.header.masters.extend(masters);
    Ok(output)
}
