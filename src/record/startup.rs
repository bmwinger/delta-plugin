/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{nt_str, subrecords_len, take_nt_str, RecordError};
use base64::{prelude::BASE64_STANDARD, Engine};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[skip_serializing_none]
#[derive(OverrideRecord, Constructor, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
// FIXME: This should be an optional field on Scripts, rather than a separate record
// That also solves the issue that Startup IDs are supposed to be the same as the ID of
// the script they reference
pub struct Startup {
    data: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Startup> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let mut id = None;
        let mut _data = None;
        for subrecord in record.subrecords() {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"DATA" => _data = Some(take_nt_str(data)?.1),
                x => return Err(RecordError::UnexpectedSubrecord(*x)),
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Startup, id),
            Startup { data: _data },
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Startup> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));
        subrecords.push(Subrecord::new(
            *b"DATA",
            BASE64_STANDARD.decode(self.record.data.unwrap_or("".to_string()))?,
            false,
        ));

        let header = RecordHeader::new(*b"SSCR", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
