/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{nt_str, push_opt_str_subrecord, subrecords_len, take_nt_str, RecordError};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_u8;
use nom::sequence::tuple;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

// Could be a delta record, however sound files and volume levels are tightly related, so
// it makes little sense to allow them to be changed independently.
#[derive(OverrideRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Sound {
    filename: Option<String>,
    volume: f64,
    min_range: f64,
    max_range: f64,
}

impl TryFrom<esplugin::Record> for RecordPair<Sound> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut filename = None;
        let mut volume = None;
        let mut min_range = None;
        let mut max_range = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => filename = Some(take_nt_str(data)?.1),
                b"DATA" => {
                    let (_, (_volume, _min_range, _max_range)) =
                        tuple((le_u8, le_u8, le_u8))(data)?;
                    volume = Some(((_volume as f64 / 0.255).round()) / 1000.0);
                    min_range = Some(((_min_range as f64 / 0.255).round()) / 1000.0);
                    max_range = Some(((_max_range as f64 / 0.255).round()) / 1000.0);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let volume = volume.ok_or(RecordError::MissingSubrecord("DATA"))?;
        let min_range = min_range.ok_or(RecordError::MissingSubrecord("DATA"))?;
        let max_range = max_range.ok_or(RecordError::MissingSubrecord("DATA"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Sound, id),
            Sound::new(filename, volume, min_range, max_range),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Sound> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        subrecords.push(Subrecord::new(*b"NAME", nt_str(&id)?, false));
        push_opt_str_subrecord(&mut subrecords, &self.record.filename, "FNAM")?;
        let mut data = vec![];
        data.extend(&((self.record.volume * 255.0).round() as u8).to_le_bytes());
        data.extend(&((self.record.min_range * 255.0).round() as u8).to_le_bytes());
        data.extend(&((self.record.max_range * 255.0).round() as u8).to_le_bytes());
        subrecords.push(Subrecord::new(*b"DATA", data, false));

        let header = RecordHeader::new(*b"SOUN", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
