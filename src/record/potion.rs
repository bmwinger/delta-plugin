/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::effect::Effect;
use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::Constructor;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_f32;
use nom_derive::Parse;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Potion {
    /// In-game name for the Potion
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// Icon identifier
    pub icon: Option<String>,
    /// Script (FIXME: When is the script run?)
    pub script: Option<String>,
    /// Weight of the Potion
    pub weight: f64,
    /// Value of the Potion
    pub value: i64,
    /// FIXME: What does this do?
    pub autocalc: bool,
    /// Effects of the potion
    pub effects: Vec<Effect>,
}

impl TryFrom<esplugin::Record> for RecordPair<Potion> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut icon = None;
        let mut potion_data = None;
        let mut effects = vec![];

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            #[nom(Parse = "le_f32")]
            weight: f32,
            value: i32,
            autocalc: i32,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"TEXT" => icon = Some(take_nt_str(data)?.1),
                b"ALDT" => {
                    potion_data = Some(Data::parse(data)?.1);
                }
                b"ENAM" => {
                    effects.push(data.try_into()?);
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = potion_data.ok_or(RecordError::MissingSubrecord("ALDT"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Potion, id),
            Potion::new(
                name,
                model,
                icon,
                script,
                parse_float(data.weight),
                data.value as i64,
                data.autocalc != 0,
                effects,
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Potion> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "TEXT")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;

        let mut data = vec![];
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        data.extend(&(self.record.autocalc as i32).to_le_bytes());
        subrecords.push(Subrecord::new(*b"ALDT", data, false));

        for effect in self.record.effects {
            subrecords.push(effect.into());
        }

        let header = RecordHeader::new(*b"ALCH", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
