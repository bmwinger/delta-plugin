/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::util::{
    push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str, RecordError,
};
use derive_more::Constructor;
use derive_more::Display;
use esplugin::{RecordHeader, Subrecord};
use nom::number::complete::le_i32;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(
    Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize, FromPrimitive,
)]
pub enum SoundGeneratorType {
    LeftFoot = 0,
    RightFoot = 1,
    SwimLeft = 2,
    SwimRight = 3,
    Moan = 4,
    Roar = 5,
    Scream = 6,
    Land = 7,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct SoundGenerator {
    /// Identifier for the creature producing the sound
    pub creature: Option<String>,
    /// Sound identifier
    pub sound: Option<String>,
    pub sound_type: SoundGeneratorType,
}

impl TryFrom<esplugin::Record> for RecordPair<SoundGenerator> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut creature = None;
        let mut sound = None;
        let mut sound_type = None;

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"CNAM" => creature = Some(take_nt_str(data)?.1),
                b"SNAM" => sound = Some(take_nt_str(data)?.1),
                b"DATA" => {
                    let itype = le_i32(data)?.1;
                    sound_type = Some(
                        FromPrimitive::from_i32(itype)
                            .ok_or(RecordError::InvalidSoundGeneratorType(itype))?,
                    );
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let sound_type = sound_type.ok_or(RecordError::MissingSubrecord("DATA"))?;
        Ok(Self::new(
            RecordId::String(RecordTypeName::SoundGenerator, id),
            SoundGenerator::new(creature, sound, sound_type),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<SoundGenerator> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.creature, "CNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.sound, "SNAM")?;
        subrecords.push(Subrecord::new(
            *b"DATA",
            (self.record.sound_type as i32).to_le_bytes().to_vec(),
            false,
        ));

        let header = RecordHeader::new(*b"SNDG", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
