/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use semver::Version;

pub const VERSION: &str = match option_env!("GIT_VERSION") {
    None => crate_version!(),
    Some(version) if version.is_empty() => crate_version!(),
    Some(version) => version,
};

pub fn get_version() -> Version {
    Version::parse(VERSION).unwrap_or_else(|_| panic!("Unable to parse version string {}", VERSION))
}
