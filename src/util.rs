/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */
use std::borrow::Cow;
use std::cell::RefCell;
use std::io;
use std::str;

use derive_more::{Display, From};
use encoding::all::{WINDOWS_1250, WINDOWS_1251, WINDOWS_1252};
use encoding::{DecoderTrap, EncoderTrap};
use esplugin::Subrecord;
use mint::{Point3, Vector3};
use nom::bytes::complete::take_while;
use nom::number::complete::le_f32;
use nom::sequence::tuple;
use nom::IResult;
use openmw_cfg::get_encoding;
use thiserror::Error;

use crate::record::RecordId;

#[derive(Error, From, Debug, Display)]
pub enum RecordError {
    YamlError(serde_yaml::Error),
    EncodingError(Cow<'static, str>),
    #[display(fmt = "Parsing Error: {:?} {:?}", _0, _1)]
    #[from(ignore)]
    ParsingError(Vec<u8>, nom::error::ErrorKind),
    #[display(fmt = "Parsing Failure: {:?} {:?}", _0, _1)]
    #[from(ignore)]
    ParsingFailure(Vec<u8>, nom::error::ErrorKind),
    #[display(fmt = "Parsing Incomplete: {:?}", _0)]
    #[from(ignore)]
    ParsingIncomplete(nom::Needed),
    #[display(fmt = "Error parsing subrecord {} of type {}", _0, _1)]
    SubrecordParsingError(String, String),
    IOError(io::Error),
    EsPluginError(esplugin::Error),
    #[from(ignore)]
    #[display(fmt = "Id: {:?} is not the correct type for its record", _0)]
    InvalidId(RecordId),
    Base64Error(base64::DecodeError),
    FromUtf8Error(std::string::FromUtf8Error),
    ParseCharError(std::char::ParseCharError),
    #[display(fmt = "Unexpected Subrecord type: {:?}", _0)]
    UnexpectedSubrecord([u8; 4]),
    #[from(ignore)]
    #[display(fmt = "Invalid Attribute ID: {}", _0)]
    InvalidAttributeId(i32),
    #[from(ignore)]
    #[display(fmt = "Invalid Specialization ID: {}", _0)]
    InvalidSpecId(i32),
    #[from(ignore)]
    #[display(fmt = "Invalid Skill ID: {}", _0)]
    InvalidSkillId(i32),
    #[from(ignore)]
    #[display(fmt = "Invalid Effect ID: {}", _0)]
    InvalidEffectId(i32),
    #[display(fmt = "Could not find name for cell reference: {}", _0)]
    UnknownCellRef(crate::record::cell_ref::CellRefId),
    CellTraitsFromBitsError(enumflags2::FromBitsError<crate::record::cell::CellTraits>),
    ContainerTraitsFromBitsError(
        enumflags2::FromBitsError<crate::record::container::ContainerTraits>,
    ),
    #[from(ignore)]
    #[display(fmt = "Record missing Subrecord of type: {}", _0)]
    MissingSubrecord(&'static str),
    #[from(ignore)]
    #[display(fmt = "Invalid Apparatus type: {}", _0)]
    InvalidApparatusType(i32),
    #[from(ignore)]
    #[display(fmt = "Invalid body part: {}", _0)]
    InvalidBodyPart(u8),
    #[from(ignore)]
    #[display(fmt = "Invalid armour type: {}", _0)]
    InvalidArmourType(i32),
    #[from(ignore)]
    #[display(fmt = "Invalid mesh type: {}", _0)]
    InvalidMeshType(u8),
    AutoCalcFlagsFromBitsError(enumflags2::FromBitsError<crate::record::class::AutoCalcFlags>),
    #[from(ignore)]
    #[display(fmt = "Invalid clothing type: {}", _0)]
    InvalidClothingType(i32),
    CreatureTraitsFromBitsError(enumflags2::FromBitsError<crate::record::creature::CreatureTraits>),
    #[from(ignore)]
    #[display(fmt = "Invalid creature type: {}", _0)]
    InvalidCreatureType(i32),
    ServicesFromBitsError(enumflags2::FromBitsError<crate::ai::Services>),
    #[from(ignore)]
    GenderFromBitsError(i8),
    #[from(ignore)]
    #[display(fmt = "Invalid Select Rule Function: {:?}", _0)]
    InvalidSelectRuleType([u8; 3]),
    #[from(ignore)]
    #[display(fmt = "Invalid range id: {}", _0)]
    InvalidRangeId(i32),
    #[from(ignore)]
    #[display(fmt = "Invalid Enchantment Type: {:?}", _0)]
    InvalidEnchantmentType(i32),
    #[from(ignore)]
    SubrecordError(String),
    #[from(ignore)]
    TypeError(String),
    LandscapeTraitsFromBitsError(
        enumflags2::FromBitsError<crate::record::landscape::LandscapeTraits>,
    ),
    LightTraitsFromBitsError(enumflags2::FromBitsError<crate::record::light::LightTraits>),
    EffectFlagsFromBitsError(enumflags2::FromBitsError<crate::record::magic_effect::EffectFlags>),
    #[from(ignore)]
    #[display(fmt = "Invalid Spell School encoding {:?}", _0)]
    SpellSchoolFromBitsError(i32),
    NPCTraitsFromBitsError(enumflags2::FromBitsError<crate::record::npc::NPCTraits>),
    #[from(ignore)]
    #[display(fmt = "Invalid SoundGenerator type: {}", _0)]
    InvalidSoundGeneratorType(i32),
    #[from(ignore)]
    #[display(fmt = "Invalid Weapon type: {}", _0)]
    InvalidWeaponType(i16),
    WeaponTraitsFromBitsError(enumflags2::FromBitsError<crate::record::weapon::WeaponTraits>),
    #[from(ignore)]
    #[display(fmt = "Invalid Spell type id: {}", _0)]
    InvalidSpellTypeId(i32),
    #[from(ignore)]
    #[display(fmt = "Unknown master: {}", _0)]
    UnknownMaster(String),
    #[from(ignore)]
    #[display(fmt = "Invalid use value index {} for skill {}", _0, _1)]
    InvalidSkillUseValueIndex(usize, crate::types::SkillType),
    #[from(ignore)]
    UnknownSkillUseValueString(String),
    /*
    #[display(fmt = "Cannot serialize record: {:?}", _0)]
    #[from(ignore)]
    InvalidRecord(RecordId),
    */
    #[from(ignore)]
    #[display(fmt = "Unknown record type: {:?}", _0)]
    UnknownRecordType([u8; 4]),

    #[from(ignore)]
    #[display(fmt = "Unknown field name {} for structure of type {}", _0, _1)]
    UnknownField(String, String),

    #[display(
        fmt = "master {} has an index of {}, which exceeds the largest index which can be stored in a cell reference (255).",
        _0,
        _1
    )]
    InvalidMasterIndex(String, u32),
}

impl From<nom::Err<nom::error::Error<&[u8]>>> for RecordError {
    fn from(other: nom::Err<nom::error::Error<&[u8]>>) -> Self {
        match other {
            nom::Err::Error(e) => RecordError::ParsingError(e.input.to_vec(), e.code),
            nom::Err::Incomplete(i) => RecordError::ParsingIncomplete(i),
            nom::Err::Failure(e) => RecordError::ParsingFailure(e.input.to_vec(), e.code),
        }
    }
}

thread_local! {
    pub static ENCODING: RefCell<Option<String>> = RefCell::new(None);
}

fn get_string_encoder() -> encoding::types::EncodingRef {
    let encoding_cached: bool = ENCODING.with(|e| e.borrow().is_some());
    if !encoding_cached {
        let encoding = get_encoding().unwrap_or_else(|_| "win1252".to_string());
        ENCODING.with(|e| {
            let mut enc = e.borrow_mut();
            *enc = Some(encoding);
        });
    }
    ENCODING.with(|e| match e.borrow().as_ref().unwrap().as_str() {
        "win1250" => WINDOWS_1250,
        "win1251" => WINDOWS_1251,
        "win1252" => WINDOWS_1252,
        _ => unimplemented!(),
    })
}

pub fn take_nt_str_exact(data: &[u8], size: usize) -> Result<(&[u8], String), RecordError> {
    let decoder = get_string_encoder();
    let (_, string) = if data.len() > size {
        take_while(|x| x != 0)(&data[0..size])?
    } else {
        take_while(|x| x != 0)(data)?
    };
    let result = decoder.decode(string, DecoderTrap::Strict)?;
    Ok((&data[size..], result))
}

pub fn take_nt_str(data: &[u8]) -> Result<(&[u8], String), RecordError> {
    let decoder = get_string_encoder();
    let (remaining, string) = take_while(|x| x != 0)(data)?;
    let result = decoder.decode(string, DecoderTrap::Strict)?;
    if !remaining.is_empty() {
        // Discard trailing zero
        Ok((&remaining[1..], result))
    } else {
        Ok((remaining, result))
    }
}

pub fn record_type(typ: &str) -> [u8; 4] {
    let mut result: [u8; 4] = [0; 4];
    result.copy_from_slice(typ.as_bytes());
    result
}

pub fn nt_str(string: &str) -> Result<Vec<u8>, RecordError> {
    let encoder = get_string_encoder();
    let result = encoder.encode(string, EncoderTrap::Strict)?;
    let mut result = result.to_vec();
    result.push(0);
    Ok(result)
}

pub fn subrecords_len(subrecords: &[Subrecord]) -> u32 {
    subrecords.iter().map(|x| x.data().len() + 8).sum::<usize>() as u32
}

pub fn parse_float(float: f32) -> f64 {
    float.to_string().parse().unwrap()
}

pub fn push_opt_str_subrecord(
    subrecords: &mut Vec<Subrecord>,
    value: &Option<String>,
    rtype: &'static str,
) -> Result<(), RecordError> {
    if let Some(string) = value {
        subrecords.push(Subrecord::new(record_type(rtype), nt_str(string)?, false));
    }
    Ok(())
}

pub fn push_str_subrecord(
    subrecords: &mut Vec<Subrecord>,
    value: &str,
    rtype: &'static str,
) -> Result<(), RecordError> {
    subrecords.push(Subrecord::new(record_type(rtype), nt_str(value)?, false));
    Ok(())
}

macro_rules! insert_nonzero {
    ($map:expr, $key: expr, $value:expr) => {
        if $value as f64 != 0.0 {
            $map.insert($key, $value);
        }
    };
}

/// Returns a floating point representation of the given byte normalized around 1.0
///
/// Rounds to three significant digits to handle the lack of precision in the original
/// storage format.
/// Can be used to parse percentages which were stored as bytes.
pub fn byte_to_float(byte: u8) -> f64 {
    ((byte as f64 / 0.255).round()) / 1000.0
}

pub fn take_point<T: From<f64> + Clone>(data: &[u8]) -> IResult<&[u8], Point3<T>> {
    let (data, (x, y, z)) = tuple((le_f32, le_f32, le_f32))(data)?;
    Ok((
        data,
        Point3::from_slice(&[
            parse_float(x).into(),
            parse_float(y).into(),
            parse_float(z).into(),
        ]),
    ))
}

pub fn take_vector<T: From<f64> + Clone>(data: &[u8]) -> IResult<&[u8], Vector3<T>> {
    let (data, (x, y, z)) = tuple((le_f32, le_f32, le_f32))(data)?;
    Ok((
        data,
        Vector3::from_slice(&[
            parse_float(x).into(),
            parse_float(y).into(),
            parse_float(z).into(),
        ]),
    ))
}

pub fn is_default<T: Default + PartialEq>(t: &T) -> bool {
    t == &T::default()
}

pub fn is_empty<K, V>(map: &hashlink::LinkedHashMap<K, V>) -> bool {
    map.is_empty()
}
