/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::float::Float;
use crate::util::{nt_str, parse_float, RecordError};
use derive_more::{Constructor, Display};
use hashlink::LinkedHashMap;
use mint::{Point3, Vector3};
use nom::number::complete::{le_f32, le_u8};
use nom::sequence::tuple;
use nom::IResult;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
use strum_macros::EnumString;

#[derive(
    Clone,
    Display,
    Eq,
    PartialEq,
    Copy,
    Debug,
    FromPrimitive,
    EnumString,
    Hash,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum SkillType {
    Block = 0,
    Armorer = 1,
    MediumArmor = 2,
    HeavyArmor = 3,
    BluntWeapon = 4,
    LongBlade = 5,
    Axe = 6,
    Spear = 7,
    Athletics = 8,
    Enchant = 9,
    Destruction = 10,
    Alteration = 11,
    Illusion = 12,
    Conjuration = 13,
    Mysticism = 14,
    Restoration = 15,
    Alchemy = 16,
    Unarmored = 17,
    Security = 18,
    Sneak = 19,
    Acrobatics = 20,
    LightArmor = 21,
    ShortBlade = 22,
    Marksman = 23,
    Mercantile = 24,
    Speechcraft = 25,
    HandToHand = 26,
}

impl TryFrom<i32> for SkillType {
    type Error = RecordError;
    fn try_from(skill: i32) -> Result<SkillType, RecordError> {
        FromPrimitive::from_i32(skill).ok_or(RecordError::InvalidSkillId(skill))
    }
}

/// Parses a slice of skills, which is expected to be exactly 27 elements long and in order
pub fn parse_skills<T: Into<u32> + Copy>(skills: &[T]) -> LinkedHashMap<SkillType, u32> {
    let mut new = LinkedHashMap::new();
    for i in 0..=(SkillType::max() as i32) {
        let skill = FromPrimitive::from_i32(i).unwrap();
        new.insert(skill, skills[i as usize].into());
    }
    new
}

impl SkillType {
    // FIXME: Replace with iterator
    pub fn max() -> usize {
        26
    }
}

#[derive(
    Clone,
    Copy,
    Display,
    Eq,
    PartialEq,
    Debug,
    FromPrimitive,
    EnumString,
    Hash,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum AttributeType {
    Strength = 0,
    Intelligence = 1,
    Willpower = 2,
    Agility = 3,
    Speed = 4,
    Endurance = 5,
    Personality = 6,
    Luck = 7,
}

impl TryFrom<i32> for AttributeType {
    type Error = RecordError;
    fn try_from(attr: i32) -> Result<AttributeType, RecordError> {
        FromPrimitive::from_i32(attr).ok_or(RecordError::InvalidAttributeId(attr))
    }
}

/// Parses a slice of attributes, which is expected to be exactly 8 elements long and in order
pub fn parse_attributes<T: Into<i32> + Copy>(
    attrs: impl Iterator<Item = T>,
) -> LinkedHashMap<AttributeType, i32> {
    let mut attrs = attrs;
    use self::AttributeType::*;
    vec![
        (Strength, attrs.next().unwrap().into()),
        (Intelligence, attrs.next().unwrap().into()),
        (Willpower, attrs.next().unwrap().into()),
        (Agility, attrs.next().unwrap().into()),
        (Speed, attrs.next().unwrap().into()),
        (Endurance, attrs.next().unwrap().into()),
        (Personality, attrs.next().unwrap().into()),
        (Luck, attrs.next().unwrap().into()),
    ]
    .into_iter()
    .collect()
}

#[derive(
    Clone,
    Copy,
    Display,
    Eq,
    PartialEq,
    Debug,
    EnumString,
    Hash,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum StatType {
    Health,
    Mana,
    Fatigue,
}

/// Parses a slice of stats, which is expected to be exactly 3 elements long and in order
pub fn parse_stats<T: Into<i32> + Copy>(stats: &[T]) -> LinkedHashMap<StatType, i32> {
    use self::StatType::*;
    vec![
        (Health, stats[0].into()),
        (Mana, stats[1].into()),
        (Fatigue, stats[2].into()),
    ]
    .into_iter()
    .collect()
}

#[derive(
    Clone,
    Copy,
    Display,
    Eq,
    Hash,
    PartialEq,
    Debug,
    FromPrimitive,
    EnumString,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum SpecializationType {
    Combat = 0,
    Magic = 1,
    Stealth = 2,
}

impl TryFrom<i32> for SpecializationType {
    type Error = RecordError;
    fn try_from(spec: i32) -> Result<SpecializationType, RecordError> {
        FromPrimitive::from_i32(spec).ok_or(RecordError::InvalidSpecId(spec))
    }
}

// Needed to allow pre-construction of objects that require a SpecializationType
impl Default for SpecializationType {
    fn default() -> Self {
        SpecializationType::Combat
    }
}

#[derive(
    Clone,
    Copy,
    Debug,
    Display,
    Eq,
    PartialEq,
    FromPrimitive,
    EnumString,
    Hash,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum SpellType {
    Spell = 0,
    Ability = 1,
    Blight = 2,
    Disease = 3,
    Curse = 4,
    Power = 5,
}

#[derive(
    Clone,
    Copy,
    Debug,
    Display,
    Eq,
    PartialEq,
    FromPrimitive,
    EnumString,
    Hash,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum SpellSchool {
    Alteration = 0,
    Conjuration = 1,
    Destruction = 2,
    Illusion = 3,
    Mysticism = 4,
    Restoration = 5,
}

#[derive(
    Clone,
    Copy,
    Debug,
    Display,
    PartialEq,
    Eq,
    FromPrimitive,
    Hash,
    EnumString,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum RangeType {
    #[strum(serialize = "Self")]
    #[display(fmt = "Self")]
    SelfType = 0,
    Touch = 1,
    Target = 2,
}

#[derive(
    Clone,
    Copy,
    Debug,
    Display,
    Eq,
    PartialEq,
    FromPrimitive,
    EnumString,
    Hash,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum EffectType {
    WaterBreathing = 0,
    SwiftSwim = 1,
    WaterWalking = 2,
    Shield = 3,
    FireShield = 4,
    LightningShield = 5,
    FrostShield = 6,
    Burden = 7,
    Feather = 8,
    Jump = 9,
    Levitate = 10,
    SlowFall = 11,
    Lock = 12,
    Open = 13,
    FireDamage = 14,
    ShockDamage = 15,
    FrostDamage = 16,
    DrainAttribute = 17,
    DrainHealth = 18,
    DrainMagicka = 19,
    DrainFatigue = 20,
    DrainSkill = 21,
    DamageAttribute = 22,
    DamageHealth = 23,
    DamageMagicka = 24,
    DamageFatigue = 25,
    DamageSkill = 26,
    Poison = 27,
    WeaknessToFire = 28,
    WeaknessToFrost = 29,
    WeaknessToShock = 30,
    WeaknessToMagicka = 31,
    WeaknessToCommonDisease = 32,
    WeaknessToBlightDisease = 33,
    WeaknessToCorprusDisease = 34,
    WeaknessToPoison = 35,
    WeaknessToNormalWeapons = 36,
    DisintegrateWeapon = 37,
    DisintegrateArmor = 38,
    Invisibility = 39,
    Chameleon = 40,
    Light = 41,
    Sanctuary = 42,
    NightEye = 43,
    Charm = 44,
    Paralyze = 45,
    Silence = 46,
    Blind = 47,
    Sound = 48,
    CalmHumanoid = 49,
    CalmCreature = 50,
    FrenzyHumanoid = 51,
    FrenzyCreature = 52,
    DemoralizeHumanoid = 53,
    DemoralizeCreature = 54,
    RallyHumanoid = 55,
    RallyCreature = 56,
    Dispel = 57,
    Soultrap = 58,
    Telekinesis = 59,
    Mark = 60,
    Recall = 61,
    DivineIntervention = 62,
    AlmsiviIntervention = 63,
    DetectAnimal = 64,
    DetectEnchantment = 65,
    DetectKey = 66,
    SpellAbsorption = 67,
    Reflect = 68,
    CureCommonDisease = 69,
    CureBlightDisease = 70,
    CureCorprusDisease = 71,
    CurePoison = 72,
    CureParalyzation = 73,
    RestoreAttribute = 74,
    RestoreHealth = 75,
    RestoreMagicka = 76,
    RestoreFatigue = 77,
    RestoreSkill = 78,
    FortifyAttribute = 79,
    FortifyHealth = 80,
    FortifyMagicka = 81,
    FortifyFatigue = 82,
    FortifySkill = 83,
    FortifyMaximumMagicka = 84,
    AbsorbAttribute = 85,
    AbsorbHealth = 86,
    AbsorbMagicka = 87,
    AbsorbFatigue = 88,
    AbsorbSkill = 89,
    ResistFire = 90,
    ResistFrost = 91,
    ResistShock = 92,
    ResistMagicka = 93,
    ResistCommonDisease = 94,
    ResistBlightDisease = 95,
    ResistCorprusDisease = 96,
    ResistPoison = 97,
    ResistNormalWeapons = 98,
    ResistParalysis = 99,
    RemoveCurse = 100,
    TurnUndead = 101,
    SummonScamp = 102,
    SummonClannfear = 103,
    SummonDaedroth = 104,
    SummonDremora = 105,
    SummonAncestralGhost = 106,
    SummonSkeletalMinion = 107,
    SummonBonewalker = 108,
    SummonGreaterBonewalker = 109,
    SummonBonelord = 110,
    SummonWingedTwilight = 111,
    SummonHunger = 112,
    SummonGoldenSaint = 113,
    SummonFlameAtronach = 114,
    SummonFrostAtronach = 115,
    SummonStormAtronach = 116,
    FortifyAttack = 117,
    CommandCreature = 118,
    CommandHumanoid = 119,
    BoundDagger = 120,
    BoundLongsword = 121,
    BoundMace = 122,
    BoundBattleAxe = 123,
    BoundSpear = 124,
    BoundLongbow = 125,
    ExtraSpell = 126,
    BoundCuirass = 127,
    BoundHelm = 128,
    BoundBoots = 129,
    BoundShield = 130,
    BoundGloves = 131,
    Corprus = 132,
    Vampirism = 133,
    SummonCenturionSphere = 134,
    SunDamage = 135,
    StuntedMagicka = 136,

    // Tribunal only
    SummonFabricant = 137,

    // Bloodmoon only
    SummonWolf = 138,
    SummonBear = 139,
    SummonBonewolf = 140,
    SummonCreature04 = 141,
    SummonCreature05 = 142,
}

impl TryFrom<i32> for EffectType {
    type Error = RecordError;
    fn try_from(effect: i32) -> Result<EffectType, RecordError> {
        FromPrimitive::from_i32(effect).ok_or(RecordError::InvalidEffectId(effect))
    }
}

#[derive(
    Clone,
    Copy,
    Debug,
    Display,
    Eq,
    PartialEq,
    FromPrimitive,
    EnumString,
    Hash,
    ToPrimitive,
    Serialize,
    Deserialize,
)]
pub enum Gender {
    Male = 0,
    Female = 1,
    None = -1,
}

#[derive(Constructor, Default, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct RGBColour<T> {
    pub red: T,
    pub blue: T,
    pub green: T,
}

#[derive(Constructor, Default, Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct RGBAColour {
    pub red: u8,
    pub blue: u8,
    pub green: u8,
    pub alpha: u8,
}

impl RGBAColour {
    pub fn to_le_bytes(self) -> [u8; 4] {
        [
            self.red.to_le(),
            self.blue.to_le(),
            self.green.to_le(),
            self.alpha.to_le(),
        ]
    }
}

pub fn take_rgba_colour(data: &[u8]) -> IResult<&[u8], RGBAColour> {
    let (data, (red, green, blue, alpha)) = tuple((le_u8, le_u8, le_u8, le_u8))(data)?;
    Ok((data, RGBAColour::new(red, green, blue, alpha)))
}

/*
pub fn take_rgb_colour(data: &[u8]) -> Result<(&[u8], RGBColour<u8>), RecordError> {
    let (data, (red, green, blue)) = tuple((le_u8, le_u8, le_u8))(data)?;
    Ok((data, RGBColour::new(red, green, blue)))
}
*/

#[derive(Copy, Constructor, Display, Eq, PartialEq, Hash, Debug, Clone, Serialize, Deserialize)]
#[display(fmt = "Point({}, {})", x, y)]
pub struct Point2<T: PartialEq + std::fmt::Debug> {
    #[serde(bound(deserialize = "T: Deserialize<'de>"))]
    pub x: T,
    #[serde(bound(deserialize = "T: Deserialize<'de>"))]
    pub y: T,
}

pub fn float_vector_to_bytes<T: Into<[Float; 3]>>(vector: T) -> Vec<u8> {
    let values: [Float; 3] = vector.into();
    values
        .iter()
        .flat_map(|x| x.f32().to_le_bytes().to_vec())
        .collect()
}

pub fn vector_to_bytes<T: Into<[f64; 3]>>(vector: T) -> Vec<u8> {
    let values: [f64; 3] = vector.into();
    values
        .iter()
        .flat_map(|x| (*x as f32).to_le_bytes().to_vec())
        .collect()
}

#[derive(Clone, Constructor, Debug, PartialEq, Serialize, Deserialize)]
pub struct Position {
    pub position: Point3<f64>,
    pub rotation: Vector3<f64>,
}

pub fn take_position(data: &[u8]) -> Result<(&[u8], Position), RecordError> {
    let (data, (x, y, z, rx, ry, rz)) =
        tuple((le_f32, le_f32, le_f32, le_f32, le_f32, le_f32))(data)?;
    Ok((
        data,
        Position::new(
            Point3::<f64> {
                x: parse_float(x),
                y: parse_float(y),
                z: parse_float(z),
            },
            Vector3::<f64> {
                x: parse_float(rx),
                y: parse_float(ry),
                z: parse_float(rz),
            },
        ),
    ))
}

#[derive(Clone, Constructor, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Transport {
    pub position: Point3<Float>,
    pub rotation: Vector3<Float>,
    pub cell_name: String,
}

impl Position {
    pub fn into_subrecord(self, typ: &[u8; 4]) -> Result<esplugin::Subrecord, RecordError> {
        let mut data = vec![];
        data.extend(&(self.position.x as f32).to_le_bytes());
        data.extend(&(self.position.y as f32).to_le_bytes());
        data.extend(&(self.position.z as f32).to_le_bytes());
        data.extend(vector_to_bytes(self.rotation));

        Ok(esplugin::Subrecord::new(*typ, data, false))
    }
}

impl TryInto<Vec<esplugin::Subrecord>> for Transport {
    type Error = RecordError;
    fn try_into(self) -> Result<Vec<esplugin::Subrecord>, RecordError> {
        let mut subs = vec![];
        let mut data = vec![];
        data.extend(&(self.position.x.f32()).to_le_bytes());
        data.extend(&(self.position.y.f32()).to_le_bytes());
        data.extend(&(self.position.z.f32()).to_le_bytes());
        data.extend(float_vector_to_bytes(self.rotation));

        subs.push(esplugin::Subrecord::new(*b"DODT", data, false));
        subs.push(esplugin::Subrecord::new(
            *b"DNAM",
            nt_str(&self.cell_name)?,
            false,
        ));
        Ok(subs)
    }
}
