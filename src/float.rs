/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Hashable float implementation inspired by https://github.com/ron-rs/ron
 */

use derive_more::{Constructor, Display, From, FromStr};
use serde::{Deserialize, Serialize};

#[derive(
    From, FromStr, Constructor, Default, Display, Copy, Clone, Debug, Serialize, Deserialize,
)]
#[display(fmt = "{}", _0)]
pub struct Float(f64);

impl Float {
    pub fn f32(self) -> f32 {
        self.into()
    }
}

impl PartialEq for Float {
    fn eq(&self, other: &Self) -> bool {
        self.0.is_nan() && other.0.is_nan() || self.0 == other.0
    }
}

impl From<Float> for f32 {
    fn from(float: Float) -> f32 {
        float.0 as f32
    }
}

impl From<f32> for Float {
    fn from(float: f32) -> Self {
        Float(float.to_string().parse().unwrap())
    }
}

impl Eq for Float {}

impl std::hash::Hash for Float {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        state.write_u64(self.0 as u64);
    }
}
